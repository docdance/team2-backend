export class CreateMemberDto {
  id: number;

  name: string;

  tel: string;

  point: number;

  created: Date;

  updated: Date;
}
