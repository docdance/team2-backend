import { IsNotEmpty } from 'class-validator';
import { Product } from 'src/products/entities/product.entity';

export class CreatePromotionDto {
  @IsNotEmpty()
  name: string;

  description: string;

  percentDiscount: number;

  priceDiscount: number;

  minQty: number;

  minPrice: number;

  product: Product;

  member: boolean;

  getPoint: number;

  usePoint: number;

  forOneItem: boolean;

  @IsNotEmpty()
  start_date: string;

  @IsNotEmpty()
  end_date: string;
}
