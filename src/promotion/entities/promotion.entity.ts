import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({ default: -1 })
  percentDiscount: number;

  @Column({ default: -1 })
  priceDiscount: number;

  @Column({ default: 1 })
  minQty: number;

  @Column({ default: -1 })
  minPrice: number;

  @Column({ default: -1 })
  getPoint: number;

  @Column({ default: -1 })
  usePoint: number;

  @Column({ default: false })
  member: boolean;

  @Column({ name: 'forOneItem', default: false })
  forOneItem: boolean;

  @Column({ type: 'date' })
  start_date: string;

  @Column({ type: 'date' })
  end_date: string;

  //productId
  @Column({ default: -1 })
  productId: number;

  // @ManyToOne(() => Product, (product) => product.promotion)
  // product: Product;

  @ManyToMany(() => Order, (order) => order.promotions, { onDelete: 'CASCADE' })
  @JoinTable()
  order: Order;
}
