import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { CheckStock } from './checkStock.entity';
import { Material } from 'src/materials/entities/material.entity';

@Entity()
export class StockDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  in_date: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qt_previous: number;

  @Column()
  quantity: number;

  @Column()
  min: number;

  @Column()
  use: number;

  @Column()
  status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => CheckStock, (checkStock) => checkStock.stockDetails, {
    onDelete: 'CASCADE',
  })
  checkStock: CheckStock;

  @ManyToOne(() => Material, (material) => material.stockDetails)
  material: Material;
}
