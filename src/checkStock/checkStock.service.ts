import { Injectable } from '@nestjs/common';
import { CreateCheckStockDto } from './dto/create-checkStock.dto';
import { UpdateCheckStockDto } from './dto/update-checkStock.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckStock } from './entities/checkStock.entity';
import { Repository } from 'typeorm';
import { StockDetail } from './entities/stockDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class CheckStockService {
  constructor(
    @InjectRepository(CheckStock)
    private checkStockRepository: Repository<CheckStock>,
    @InjectRepository(StockDetail)
    private stockDetailsRepository: Repository<StockDetail>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  async create(createCheckStockDto: CreateCheckStockDto) {
    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString('en-GB');
    const checkStock = new CheckStock();

    const user = await this.usersRepository.findOneBy({
      id: createCheckStockDto.userId,
    });
    createCheckStockDto.userId = user.id;
    checkStock.user = user;
    checkStock.userName = user.fullName;
    checkStock.date = formattedDate;
    checkStock.totalPrice = 0;
    checkStock.totalQty = 0;
    checkStock.totalUse = 0;
    checkStock.stockDetails = [];

    for (const oi of createCheckStockDto.stockDetails) {
      const stockDetail = new StockDetail();

      stockDetail.material = await this.materialsRepository.findOneBy({
        id: oi.materialId,
      });
      stockDetail.in_date = formattedDate;
      stockDetail.name = stockDetail.material.name;
      stockDetail.price = stockDetail.material.price;
      stockDetail.qt_previous = stockDetail.material.quantity;
      stockDetail.quantity = oi.quantity;
      stockDetail.min = stockDetail.material.min;
      stockDetail.use = stockDetail.quantity - stockDetail.qt_previous;
      if (stockDetail.quantity < stockDetail.min) {
        stockDetail.status = 'Low';
      } else {
        stockDetail.status = 'Available';
      }
      await this.stockDetailsRepository.save(stockDetail);

      checkStock.stockDetails.push(stockDetail);
      checkStock.totalPrice += stockDetail.price * stockDetail.quantity;
      checkStock.totalQty += stockDetail.quantity;
      checkStock.totalUse += stockDetail.use * -1;
    }
    return this.checkStockRepository.save(checkStock);
  }

  findAll() {
    return this.checkStockRepository.find({});
  }

  findOne(id: number) {
    return this.checkStockRepository.findOneOrFail({
      where: { id },
      relations: { stockDetails: true },
    });
  }

  update(id: number, updateCheckStockDto: UpdateCheckStockDto) {
    return `This action updates a #${id} checkStock`;
  }

  async remove(id: number) {
    const deleteCheckStock = await this.checkStockRepository.findOneOrFail({
      where: { id },
    });
    await this.checkStockRepository.remove(deleteCheckStock);

    return deleteCheckStock;
  }
}
