export class CreateCheckStockDto {
  stockDetails: {
    materialId: number;
    quantity: number;
  }[];
  userId: number;
}
