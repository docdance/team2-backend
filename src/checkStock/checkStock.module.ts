import { Module } from '@nestjs/common';
import { CheckStockService } from './checkStock.service';
import { CheckStockController } from './checkStock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckStock } from './entities/checkStock.entity';
import { StockDetail } from './entities/stockDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/materials/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CheckStock, StockDetail, User, Material]),
  ],
  controllers: [CheckStockController],
  providers: [CheckStockService],
})
export class CheckStockModule {}
