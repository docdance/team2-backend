import { StockDetail } from 'src/checkStock/entities/stockDetail.entity';
import { OrderDetail } from 'src/materialOrders/entities/orderDetail.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  in_date: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qt_previous: number;

  @Column()
  quantity: number;

  @Column()
  min: number;

  @Column()
  use: number;

  @Column()
  status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => StockDetail, (stockDetail) => stockDetail.material)
  stockDetails: StockDetail[];

  @OneToMany(() => OrderDetail, (matOrderDetail) => matOrderDetail.material)
  matOrderDetail: OrderDetail[];
}
