import { Member } from 'src/members/entities/member.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';

export class CreateOrderDto {
  orderItemsIdQty: {
    productId: number;
    qty: number;
  }[];
  userId: number;
  cash: number;
  discount: number;
  getPoint: number;
  usePoint: number;
  payment: string;
  promotions: Promotion[];
  member: Member;
  total: number;
}
