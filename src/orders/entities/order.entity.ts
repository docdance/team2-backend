import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/members/entities/member.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('decimal', { precision: 10, scale: 2 })
  total: number;

  @Column()
  qty: number;

  @Column({
    default: 0,
  })
  cash: number;

  @Column({ default: 0, type: 'decimal', precision: 10, scale: 2 })
  discount: number;

  @Column({
    default: -1,
  })
  getPoint: number;

  @Column({
    default: -1,
  })
  usePoint: number;

  @Column({
    name: 'payment',
  })
  payment: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @ManyToMany(() => Promotion, (promotion) => promotion.order)
  promotions: Promotion[];

  @ManyToOne(() => User, (user) => user.orders)
  user: User;

  @ManyToOne(() => Member, (member) => member.orders)
  member: Member;
}
