import { CheckStock } from 'src/checkStock/entities/checkStock.entity';
import { materialOrder } from 'src/materialOrders/entities/materialOrder.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @Column({
    name: 'tel_no',
    default: '',
  })
  tel: string;

  @Column({
    name: 'base_salary',
    default: 0,
  })
  baseSalary: number;

  @Column({
    name: 'bank_name',
    default: '',
  })
  bankName: string;

  @Column({
    name: 'bank_account',
    default: '',
  })
  bankAccount: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => CheckStock, (checkStock) => checkStock.user)
  checkStocks: CheckStock[];

  @OneToMany(() => materialOrder, (order) => order.user)
  purOrders: materialOrder[];
}
