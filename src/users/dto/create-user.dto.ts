export class CreateUserDto {
  image: string;
  email: string;
  password: string;
  fullName: string;
  tel: string;
  gender: string;
  roles: string;
  baseSalary: number;
  bankName: string;
  bankAccount: string;
}
