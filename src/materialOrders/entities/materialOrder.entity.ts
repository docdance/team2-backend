import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { User } from 'src/users/entities/user.entity';
import { OrderDetail } from './orderDetail.entity';
import { Vender } from 'src/venders/entities/vender.entity';

@Entity()
export class materialOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  total: number;

  @Column()
  quantity: number;

  @Column()
  nameStore: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Vender, (vender) => vender.orders)
  vender: Vender;

  @OneToMany(() => OrderDetail, (orderDetail) => orderDetail.matOrder)
  orderDetails: OrderDetail[];

  @ManyToOne(() => User, (user) => user.purOrders)
  user: User;
}
