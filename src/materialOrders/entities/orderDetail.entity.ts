import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { materialOrder } from './materialOrder.entity';
import { Material } from 'src/materials/entities/material.entity';

@Entity()
export class OrderDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  quantity: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => materialOrder, (matOrder) => matOrder.orderDetails, {
    onDelete: 'CASCADE',
  })
  matOrder: materialOrder;

  @ManyToOne(() => Material, (material) => material.matOrderDetail)
  material: Material;
}
