import { PartialType } from '@nestjs/mapped-types';
import { CreateMatOrderDto } from './create-materialOrder.dto';

export class UpdateMatOrderDto extends PartialType(CreateMatOrderDto) {}
