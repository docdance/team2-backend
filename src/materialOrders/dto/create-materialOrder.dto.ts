export class CreateMatOrderDto {
  orderDetails: {
    materialId: number;
    quantity: number;
  }[];

  userId: number;
}
