import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { matOrdersService } from './materialOrders.service';
import { CreateMatOrderDto } from './dto/create-materialOrder.dto';
import { UpdateMatOrderDto } from './dto/update-materialOrder.dto';

@Controller('materialOrders')
export class materialOrdersController {
  constructor(private readonly matOrderService: matOrdersService) {}

  @Post()
  create(@Body() createMatOrderDto: CreateMatOrderDto) {
    return this.matOrderService.create(createMatOrderDto);
  }

  @Get()
  findAll() {
    return this.matOrderService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.matOrderService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMatOrderDto: UpdateMatOrderDto,
  ) {
    return this.matOrderService.update(+id, updateMatOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.matOrderService.remove(+id);
  }
}
