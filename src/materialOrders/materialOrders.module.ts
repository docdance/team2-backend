import { Module } from '@nestjs/common';

import { materialOrdersController } from './materialOrders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { materialOrder } from './entities/materialOrder.entity';

import { User } from 'src/users/entities/user.entity';

import { Vender } from 'src/venders/entities/vender.entity';
import { OrderDetail } from './entities/orderDetail.entity';
import { matOrdersService } from './materialOrders.service';
import { Material } from 'src/materials/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      materialOrder,
      OrderDetail,
      User,
      Vender,
      Material,
    ]),
  ],
  controllers: [materialOrdersController],
  providers: [matOrdersService],
})
export class materialOrdersModule {}
