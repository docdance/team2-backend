import { Test, TestingModule } from '@nestjs/testing';
import { materialOrdersController } from './materialOrders.controller';
import { matOrdersService } from './materialOrders.service';

describe('materialOrdersController', () => {
  let controller: materialOrdersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [materialOrdersController],
      providers: [matOrdersService],
    }).compile();

    controller = module.get<materialOrdersController>(materialOrdersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
