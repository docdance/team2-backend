import { Test, TestingModule } from '@nestjs/testing';
import { matOrdersService } from './materialOrders.service';

describe('materialOrdersService', () => {
  let service: matOrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [matOrdersService],
    }).compile();

    service = module.get<matOrdersService>(matOrdersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
