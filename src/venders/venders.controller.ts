import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { VendersService } from './venders.service';
import { CreateVenderDto } from './dto/create-vender.dto';
import { UpdateVenderDto } from './dto/update-vender.dto';

import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

@Controller('venders')
export class VendersController {
  constructor(private readonly vendersService: VendersService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/venders',
        filename: (reg, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createVenderDto: CreateVenderDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createVenderDto.image = file.fieldname;
    }

    return this.vendersService.create(createVenderDto);
  }

  @Get()
  findAll() {
    return this.vendersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.vendersService.findOne(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/venders',
        filename: (reg, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateVenderDto: UpdateVenderDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(file);

    if (file) {
      updateVenderDto.image = file.fieldname;
    }
    console.log(updateVenderDto);

    return this.vendersService.update(+id, updateVenderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vendersService.remove(+id);
  }
  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/venders',
        filename: (reg, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body() vender: { name: string; age: number },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(vender);

    console.log(file.filename);
    console.log(file.path);
  }
}
