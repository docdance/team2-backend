import { Module } from '@nestjs/common';
import { VendersService } from './venders.service';
import { VendersController } from './venders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vender } from './entities/vender.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Vender])],
  controllers: [VendersController],
  providers: [VendersService],
  exports: [VendersService],
})
export class VendersModule {}
