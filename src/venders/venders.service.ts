import { Injectable } from '@nestjs/common';
import { CreateVenderDto } from './dto/create-vender.dto';
import { UpdateVenderDto } from './dto/update-vender.dto';
import { Repository } from 'typeorm';
import { Vender } from './entities/vender.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class VendersService {
  constructor(
    @InjectRepository(Vender) private vendersRepository: Repository<Vender>,
  ) {}
  create(createVenderDto: CreateVenderDto) {
    const vender = new Vender();
    vender.name = createVenderDto.name;
    vender.tel = createVenderDto.tel;

    if (createVenderDto.image && createVenderDto.image !== '') {
      vender.image = createVenderDto.image;
    }

    return this.vendersRepository.save(vender);
  }

  findAll() {
    return this.vendersRepository.find();
  }

  findOne(id: number) {
    return this.vendersRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateVenderDto: UpdateVenderDto) {
    const vender = new Vender();
    vender.name = updateVenderDto.name;
    vender.tel = updateVenderDto.tel;

    if (updateVenderDto.image && updateVenderDto.image !== '') {
      vender.image = updateVenderDto.image;
    }
    const updateVender = await this.vendersRepository.findOneOrFail({
      where: { id },
    });
    updateVender.name = vender.name;
    updateVender.tel = vender.tel;

    updateVender.image = vender.image;

    await this.vendersRepository.save(updateVender);
    const result = await this.vendersRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteVender = await this.vendersRepository.findOneOrFail({
      where: { id },
    });
    await this.vendersRepository.remove(deleteVender);

    return deleteVender;
  }
  findOneByName(name: string) {
    return this.vendersRepository.findOneOrFail({ where: { name } });
  }
}
