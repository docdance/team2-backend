import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Product } from './products/entities/product.entity';
import { Promotion } from './promotion/entities/promotion.entity';
import { PromotionsModule } from './promotion/promotions.module';
import { ProductsModule } from './products/products.module';
import { Material } from './materials/entities/material.entity';
import { MaterialsModule } from './materials/materials.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { Role } from './roles/entities/role.entity';
import { AuthModule } from './auth/auth.module';
import { User } from './users/entities/user.entity';
import { RolesModule } from './roles/roles.module';
import { Type } from './types/entities/type.entity';
import { TypesModule } from './types/types.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { OrdersModule } from './orders/orders.module';
import { CheckStock } from './checkStock/entities/checkStock.entity';
import { StockDetail } from './checkStock/entities/stockDetail.entity';
import { CheckStockModule } from './checkStock/checkStock.module';
import { Vender } from './venders/entities/vender.entity';
import { materialOrder } from './materialOrders/entities/materialOrder.entity';
import { OrderDetail } from './materialOrders/entities/orderDetail.entity';
import { VendersModule } from './venders/venders.module';
import { materialOrdersModule } from './materialOrders/materialOrders.module';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      logging: false,
      entities: [
        Promotion,
        Product,
        Material,
        User,
        Role,
        Type,
        Order,
        OrderItem,
        CheckStock,
        StockDetail,
        Vender,
        materialOrder,
        OrderDetail,
        Member,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    ProductsModule,
    MaterialsModule,
    PromotionsModule,
    RolesModule,
    AuthModule,
    OrdersModule,
    TypesModule,
    CheckStockModule,
    materialOrdersModule,
    VendersModule,
    MembersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
